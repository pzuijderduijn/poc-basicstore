var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'basic-expressmongo'
    },
    port: 3000,
    ip: process.env.IP,
    db: 'mongodb://localhost/basic-expressmongo-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'basic-expressmongo'
    },
    port: 3000,
    ip: process.env.IP,
    db: 'mongodb://localhost/basic-expressmongo-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'basic-expressmongo'
    },
    port: 3000,
    ip: process.env.IP,
    db: 'mongodb://localhost/basic-expressmongo-production'
  }
};

module.exports = config[env];
