/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app){

    app.use('/api/preferences', require('./api/preferences'));

    // All other routes should return a 404
    app.route('/*').get(errors[404]);
};