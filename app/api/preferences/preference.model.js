// Example model

var mongoose = require('mongoose');
var schema = mongoose.Schema;

var preferenceSchema = new schema({
  userId: { type: Number, index: true},
  landingPage: {
    key : String
  },
  familyDeDuplication : String,
  searchResults : {
    key: Number
  },
  documentView : String,
  searchAuthorities: [String],
  familyAuthorities: [{
    _id:false,
    order: Number,
    authority: String
  }],
  resultColumns:[{
    _id:false,
    fixed: Boolean,
    column: String,
    resultField: String
  }]
});

preferenceSchema.set('autoIndex', false);

module.exports = mongoose.model('Preference', preferenceSchema);

