var preference = require('./preference.model');

exports.create = function(req, res, next) {
  preference.update({userId: req.body.userId}, req.body, {upsert: true}, function(err){
    if (err) return next(err);
    res.status(200).json(req.body);
  });
};

exports.show = function (req, res, next) {
  preference.findOne({userId: req.params.id}, function (err, prefs) {
    if (err) return next(err);
    res.status(200).json(prefs);
  });
};

exports.update = function (req, res, next) {
  preference.findOneAndUpdate({userId: req.params.id}, req.body, function (err, prefs) {
    if (err) return next(err);
    res.status(200).json(prefs);
  });
};

exports.delete = function(req, res){
  preference.findOneAndRemove({userId: req.params.id}, function(err) {
    if(err) return res.status(500).send(err);
    return res.send(204);
  });
};

